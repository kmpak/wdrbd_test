#pragma once
#include "drbd_windows.h"
//#include "drbd_int.h"

#define PERF_LOG_COUNT		128
#define FUNCTION_NAME_SIZE	24

typedef struct _perf_log
{
	int			index;
	char		name[32];
	ULONG_PTR	time;
	void	(*print)(void *);
} perf_log, *pperf_log;

typedef struct _ring_buffer_log
{
	perf_log	perf_log;

	unsigned int length;
	unsigned int read_pos;
	unsigned int write_pos;
	int que;
	int seq;
	unsigned int sk_wmem_queued;
	int size;
} ring_buffer_log, *pring_buffer_log;

typedef struct _req_log
{
	perf_log			perf_log;

	char				letter;
	int					what;
	void *				address;
	void *				req;
} req_log, *preq_log;

extern int perf_history_enabled;

extern int init_perf_history();
extern pperf_log write_perf_log(char * name);
#if 0
extern int insert_ring_buffer_log(pring_buffer_log plog, void * pbuf, int size);
#define INSERT_RING_BUFFER_LOG(pbuf, size)	\
	do {	\
		pperf_log plog = write_perf_log(__FUNCTION__);	\
		plog->print = print_ring_buffer_log;	\
		insert_ring_buffer_log(plog, pbuf, size);	\
	} while(0)
extern void print_ring_buffer_log(void * p);
#endif
extern int insert_write_request_log(preq_log plog, void * pbuf, int what);
#define INSERT_WRITE_REQUEST_LOG(pbuf, what)	\
	do {	\
		pperf_log plog = write_perf_log(__FUNCTION__);	\
		plog->print = print_write_request_log;	\
		insert_write_request_log(plog, pbuf, what);	\
	} while(0)
extern void print_write_request_log(void * p);
