#include "wdrbd_perf.h"
#include "drbd_int.h"

int perf_index = 0;
int perf_history_flip = 0;
int perf_history_enabled = 0;
WORK_QUEUE_ITEM perf_work_q;
pperf_log perf_history[2][PERF_LOG_COUNT];
pperf_log * perf_history_backup = NULL;
KSPIN_LOCK perf_spinlock;
KIRQL      perf_spinlock_irql;


const char * drbd_req_event_str[] = {
	"CREATED",
	"TO_BE_SENT",
	"TO_BE_SUBMITTED",
	"QUEUE_FOR_NET_WRITE",
	"QUEUE_FOR_NET_READ",
	"QUEUE_FOR_SEND_OOS",
	"QUEUE_AS_DRBD_BARRIER",
	"SEND_CANCELED",
	"SEND_FAILED",
	"HANDED_OVER_TO_NETWORK",
	"OOS_HANDED_TO_NETWORK",
	"CONNECTION_LOST_WHILE_PENDING",
	"READ_RETRY_REMOTE_CANCELED",
	"RECV_ACKED_BY_PEER",
	"WRITE_ACKED_BY_PEER",
	"WRITE_ACKED_BY_PEER_AND_SIS",
	"DISCARD_WRITE",
	"POSTPONE_WRITE",
	"NEG_ACKED",
	"BARRIER_ACKED", /* in protocol A and B */
	"DATA_RECEIVED", /* (remote read) */

	"COMPLETED_OK",
	"READ_COMPLETED_WITH_ERROR",
	"READ_AHEAD_COMPLETED_WITH_ERROR",
	"WRITE_COMPLETED_WITH_ERROR",
	"DISCARD_COMPLETED_NOTSUPP",
	"DISCARD_COMPLETED_WITH_ERROR",

	"ABORT_DISK_IO",
	"RESEND",
	"FAIL_FROZEN_DISK_IO",
	"RESTART_FROZEN_DISK_IO",
    "NOTHING"
};



void print_perf_history(void * context)
{
	for (int i = 0; i < PERF_LOG_COUNT; ++i)
	{
		pperf_log plog = perf_history_backup[i];
		plog->print(plog);
	}
}

int init_perf_history()
{
	for (int j = 0; j < 2; ++j)
	{
		for (int i = 0; i < PERF_LOG_COUNT; ++i)
		{
			pperf_log log = ExAllocatePoolWithTag(NonPagedPool, 2048, '3ADW');
			memset(log, 0, 2048);
			log->index = i;
			perf_history[j][i] = log;
		}
	}

	KeInitializeSpinLock(&perf_spinlock);

	return 0;
}

static pperf_log get_next_perf_log()
{
	KeAcquireSpinLock(&perf_spinlock, &perf_spinlock_irql);
	pperf_log log = perf_history[perf_history_flip][perf_index++];
	perf_index %= PERF_LOG_COUNT;
	if (0 == perf_index)
	{
		perf_history_backup = perf_history[perf_history_flip];
		++perf_history_flip;
		perf_history_flip %= 2;
		KeReleaseSpinLock(&perf_spinlock, perf_spinlock_irql);
	}
	else if (1 == perf_index && perf_history_backup)
	{
		KeReleaseSpinLock(&perf_spinlock, perf_spinlock_irql);

		if (perf_history_enabled)
		{
			ExInitializeWorkItem(&perf_work_q,
				print_perf_history,
				perf_history_backup);

			ExQueueWorkItem(&perf_work_q, DelayedWorkQueue);
		}
	}
	else
	{
		KeReleaseSpinLock(&perf_spinlock, perf_spinlock_irql);
	}

	return log;
}

pperf_log write_perf_log(char * name)
{
	size_t size = strlen(name);
	size = (size > FUNCTION_NAME_SIZE - 1) ? FUNCTION_NAME_SIZE - 1 : size;

	pperf_log plog = get_next_perf_log();

	strncpy(plog->name, name, size);
	plog->name[FUNCTION_NAME_SIZE - 1] = '\0';
	plog->time = jiffies;

	return plog;
}
#if 0
int insert_ring_buffer_log(pring_buffer_log plog, void * pbuf, int size)
{
	struct ring_buffer * rb = pbuf;

	plog->length = rb->length;
	plog->read_pos = rb->read_pos;
	plog->write_pos = rb->write_pos;
	plog->que = rb->que;
	plog->seq = rb->seq;
	plog->sk_wmem_queued = rb->sk_wmem_queued;
	plog->size = size;
	
	return ((pperf_log)plog)->index;
}

void print_ring_buffer_log(void * p)
{
	pring_buffer_log plog = p;

	DbgPrintEx(FLTR_COMPONENT, DPFLTR_INFO_LEVEL,
		"[%24s, %d] length(%d) read(%d) write(%d) q(%d) seq(%d) skq(%d) size(%d)\n",
		((pperf_log)plog)->name, ((pperf_log)plog)->time,
		plog->length,
		plog->read_pos,
		plog->write_pos,
		plog->que,
		plog->seq,
		plog->sk_wmem_queued,
		plog->size
	);
}
#endif
int insert_write_request_log(preq_log plog, void * pbuf, int what)
{
	memcpy(&plog->req, pbuf, sizeof(struct drbd_request));
	plog->letter = 'D';
	plog->what = what;
	plog->address = pbuf;
	return 0;
}

static void __DbgPrint_mod_rq_state(preq_log plog, struct drbd_request * r)
{
	DbgPrintEx(FLTR_COMPONENT, DPFLTR_INFO_LEVEL,
		"[%24s, %lu] %c: req(0x%p) kref(%d) completion_ref(%d) rq_state[0](0x%x) [%d](0x%x)\n",
		((pperf_log)plog)->name, ((pperf_log)plog)->time,
		plog->letter,
		plog->address,

		r->kref.refcount,
		r->completion_ref,

		r->rq_state[0], plog->what, r->rq_state[plog->what]
	);
}

static void __DbgPrint_write_request(preq_log plog, struct drbd_request * r)
{
	DbgPrintEx(FLTR_COMPONENT, DPFLTR_INFO_LEVEL,
		"[%24s, %lu] %c: req dagtag_sec(0x%llx) sec(0x%llx) size(%d) kref(%d) cref(%d) epoch(%d) rq_state[0x%x, 0x%x] "
/*		"start(%lu) in_actlog(%lu) pre_submit(%lu) pre_send(%lu) acked(%lu) net_done(%lu) "*/
		"mod(%s)\n",
		((pperf_log)plog)->name, ((pperf_log)plog)->time,
		plog->letter,

		r->dagtag_sector,
		r->i.sector,
		r->i.size,

		r->kref.refcount,
		r->completion_ref,
		r->epoch,

		r->rq_state[0], r->rq_state[1],
		/*
		r->start_jif,
		r->in_actlog_jif,
		r->pre_submit_jif,
		r->pre_send_jif[0],
		r->acked_jif[0],
		r->net_done_jif[0],
		*/
		(plog->what != -1) ? drbd_req_event_str[plog->what] : ""
	);
}

static void __DbgPrint_write_request_no_mod(preq_log plog, struct drbd_request * r)
{
	DbgPrintEx(FLTR_COMPONENT, DPFLTR_INFO_LEVEL,
		"[%24s, %lu] %c: (%d)req dagtag_sec(0x%llx) sec(0x%llx) size(%d) kref(%d) cref(%d) epoch(%d) rq_state[0x%x, 0x%x] "
		"\n",
		((pperf_log)plog)->name, ((pperf_log)plog)->time,
		plog->letter,

		r->seq,
		r->dagtag_sector,
		r->i.sector,
		r->i.size,

		r->kref.refcount,
		r->completion_ref,
		r->epoch,

		r->rq_state[0], r->rq_state[1]
	);
}

void print_write_request_log(void * p)
{
	preq_log plog = p;
	struct drbd_request * r = &plog->req;

	if (!strcmp(((pperf_log)plog)->name, "mod_rq_state"))
		__DbgPrint_mod_rq_state(p, r);
	else if (plog->what == -1)
		__DbgPrint_write_request_no_mod(p, r);
	else
		__DbgPrint_write_request(p, r);
}
