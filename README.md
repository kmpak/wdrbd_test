# README #

## Windows DRBD v9.x ##

### 주요 개선 기능 ###
* 1 : N 동기화 지원

### 개발 및 빌드 환경 ###
* Visual Studio 2013
* WDK 8.1
* Cygwin

### Contact ###
* 맨텍 WDRBD 개발팀
* [맨텍 홈페이지](www.mantech.co.kr/)